import 'package:chopper/chopper.dart';
part 'jsonplaceholder_api_service.chopper.dart';


@ChopperApi(baseUrl: 'https://jsonplaceholder.typicode.com/')
abstract class PostApiService extends ChopperService {

  @Get(path: '/{category}')
  // Query parameters are specified the same way as @Path
  // but obviously with a @Query annotation
  Future<Response> getPost(@Path('category')String category);

  static PostApiService create() {
    final client = ChopperClient(
        baseUrl: "https://jsonplaceholder.typicode.com/",
        services: [
          _$PostApiService(),
        ],
        interceptors: [
          HeadersInterceptor({"Cache-Control": "no-cache"}),
          HttpLoggingInterceptor(),
          CurlInterceptor()
        ],
        converter: JsonConverter());
    return _$PostApiService(client);
  }

}