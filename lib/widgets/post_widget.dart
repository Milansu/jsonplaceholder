import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jsonplaceholder/bloc/post_bloc.dart';

import '../bloc/post_event.dart';
import '../bloc/post_state.dart';

class PostWidget extends StatefulWidget {
  final String category;

  const PostWidget({Key key, this.category}) : super(key: key);

  @override
  _PostWidgetState createState() => _PostWidgetState();
}

class _PostWidgetState extends State<PostWidget> with AutomaticKeepAliveClientMixin {
  final PostBloc postBloc = PostBloc();

  @override
  void didChangeDependencies() {
    if (widget.category == null)
      postBloc..add(GetPost(("todos")));
    else
      postBloc..add(GetPost((widget.category)));
    super.didChangeDependencies();
  }


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<PostBloc,PostState>(bloc: postBloc,builder: (context,state) {
      if(state is PostLoaded) {
        return ListView.builder(itemBuilder: (context, index) {
          return ListTile(
            title: Text(state.post[index]["title"]),
            leading: Text(state.post[index]["id"].toString()),
          );
        });
      }
      else if(state is PostLoading) {
        return Center(child: CircularProgressIndicator());
      }
      else if(state is InitialPostState) {
        return Center(child: Text("initial state"));
      }
      else {
        return Center(child: Text("State not specified"));
      }
    },);
  }

  @override
  bool get wantKeepAlive => true;
}
