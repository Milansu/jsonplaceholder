import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jsonplaceholder/widgets/post_widget.dart';

import 'bloc/post_bloc.dart';
import 'screens/main_screen.dart';
import 'package:logging/logging.dart';

void main() {
  _setupLogging();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }

}

void _setupLogging() {
  Logger.root.level = Level.ALL;
  Logger.root.onRecord.listen((rec) {
    print("${rec.level.name}: ${rec.time}: ${rec.message}");
  });
}



class MyAppState extends State<MyApp> {

  MyAppState createState() => MyAppState();

  @override
  Widget build(BuildContext context) {
   return MultiBlocProvider(
        providers: [
          BlocProvider(
            builder: (context) => PostBloc(),
            child: PostWidget(),
          ),
        ],
        child: MaterialApp(
          title: "Jsonplaceholder.typicode.com",
          home: BlocProvider(
            builder: (context) => PostBloc(),
            child: MainScreen(),
          ),
        ),
      );
  }
}

