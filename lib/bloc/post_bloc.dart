import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:jsonplaceholder/services/jsonplaceholder_api_service.dart';
import './bloc.dart';

class PostBloc extends Bloc<PostEvent, PostState> {
  @override
  PostState get initialState => InitialPostState();

  Future<dynamic> getPosts(String category) async {
    final postApiService = await PostApiService.create().getPost(category);
    dynamic map = json.decode(postApiService.bodyString);
    return map;
  }

  @override
  Stream<PostState> mapEventToState(
    PostEvent event,
  ) async* {
    final currentState = state;
    try {
    if(event is GetPost) {
      if(currentState is InitialPostState) {
      yield PostLoading();
      final post = await getPosts(event.category);
      yield PostLoaded(post);
      }
      else if(currentState is PostLoaded) {
        yield PostLoading();
        final post = await getPosts(event.category);
        yield PostLoaded(post);
      }
    }
  }
  catch(error) {
    print(error);}
  }
}
