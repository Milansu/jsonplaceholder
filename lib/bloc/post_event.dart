import 'package:meta/meta.dart';

@immutable
abstract class PostEvent {}

class GetPost extends PostEvent {
  final String category;

  GetPost(this.category);

  @override
  // TODO: implement props
  List<Object> get props => [category];
}
