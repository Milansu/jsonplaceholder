
import 'package:meta/meta.dart';

@immutable
abstract class PostState {}

class InitialPostState extends PostState {}

class PostLoading extends PostState {
  PostLoading();
}

class PostLoaded extends PostState {
  final dynamic post;

  PostLoaded(this.post);
}


