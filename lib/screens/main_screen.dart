import 'package:flutter/material.dart';
import 'package:jsonplaceholder/widgets/post_widget.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();

}
class _MainScreenState extends State<MainScreen> {
  final categories = ["To-Dos", "Albums", "Posts"];
  final categoriesApi = ["todos","albums","posts"];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
          appBar: AppBar(
            title: Text("JsonPlaceholder"),
            bottom: TabBar(
              tabs: tabList(categories),
              isScrollable: true,
            ),
          ),
          body: TabBarView(
            children: postListScreen(categories, categoriesApi)),
        ));
  }

  List<Tab> tabList(List<String> list) {
    List<Tab> tabList = [];
    for (int i = 0; i < list.length; i++) {
      tabList.add(Tab(
        text: list[i],
      ));
    }
    return tabList;
  }

  List<Widget> postListScreen(List<String> list,
      List<String> apiList) {
    List<Widget> postList = [];
    for (int i = 0; i < list.length; i++) {
      postList.add(
          PostWidget(category: categoriesApi[i]));
    }
    return postList;
  }

}
